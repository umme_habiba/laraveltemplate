<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoryController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('/home', [PublicController::class,'home']);
Route::get('/category', [PublicController::class,'category']);
Route::get('/details', [PublicController::class,'details']);
Route::get('/cart', [ShopController::class,'cart']);
Route::get('/checkout', [ShopController::class,'checkout']);
Route::get('/placeOrder', [ShopController::class,'placeOrder']);

Route::middleware('auth')->prefix('admin')->group(function() {
//Admin
//Route::get('/admin/login',[AdminController::class,'login']);
Route::get('/',[AdminController::class,'admin']);
//product
Route::get('/view',[AdminController::class,'view']);
Route::get('/index',[AdminController::class,'product']);
//categories
Route::get('/categories',[CategoryController::class,'index'])->name('categories.index');
Route::get('/categories/create',[CategoryController::class,'create'])->name('categories.create');
Route::post('/categories',[CategoryController::class,'store'])->name('categories.store');
Route::get('/categories/{category}',[CategoryController::class,'show'])->name('categories.show');
Route::get('/categories/{category}/edit',[CategoryController::class,'edit'])->name('categories.edit');

Route::patch('/categories/{category}/edit',[CategoryController::class,'update'])->name('categories.update');

Route::delete('/categories/{category}',[CategoryController::class,'destroy'])->name('categories.destroy');


});