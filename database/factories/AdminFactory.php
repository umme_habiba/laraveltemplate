<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class AdminFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [

            //rand(1,100),
            'role_id' =>rand(1,3),
            'name' => $this->faker->name(),
            'email' => $this->faker->sentence(),
            'password' => '$2y$10$92IX'
        ];
    }

   
}
