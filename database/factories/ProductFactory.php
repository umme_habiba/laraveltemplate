<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [

            
            'categories_id' =>rand(1,3),
            'title' => $this->faker->sentence(),
            'image' => $this->faker->word(),
            'price' => 500,
            'specification' =>$this->faker->text, 
            'details' => $this->faker->text
            
        ];
    }

   
}
