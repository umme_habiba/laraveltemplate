<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role :: create([
            'title'=>'admin'

        ]);
        Role :: create([
            'title'=>'user'

        ]);
        Role :: create([
            'title'=>'guest'

        ]);
    }
}
