<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => 'required|unique:courses|max:255|min:3',
            'batch_no' => 'required',
            'class_start_date'=>'required',
            'class_end_date'=>'required',
            'instructor_name'=>'required|max:255|min:3',
            'course_type'=>'required'


           
      
        ];
    }
}
