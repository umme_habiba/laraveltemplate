<?php

namespace App\Http\Controllers;
use App\Models\Course;

use Illuminate\Http\Request;
use App\Http\Requests\CourseRequest;

class CourseController extends Controller
{
    public function index()
    {   
        $courses= Course::all();
        return view('courses.index',[
            'courses'=>$courses
        ]);
    }

    public function create()
    {
        return view('courses.create');
    }
    public function store(CourseRequest $request)
    {
        dd($request->all());
        Course::create([
            'title' => $request->title,
            'batch_no' => $request->batch_no,
            'class_start_date' => $request->class_start_date,
            'class_end_date' => $request->class_end_date,
            'instructor_name' => $request->instructor_name,
            'is_active'=>$request->is_active ? true : false,
            'course_type' => $request->course_type,


        ]);
        return redirect()->route('courses.index')->withMessage('succesfully created');
    }
}

