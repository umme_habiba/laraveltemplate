<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\RequestCategory;

use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $categories= Category::all();
        //dd($categories);
        return view('categories.index',[
            'categories'=>$categories
        ]);
        
    }
    public function create()
    {
       
        return view('categories.create');
        
    }
    public function store(RequestCategory $request)
    {
        //validation
        /*
        //return error dekhay
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:categories|max:255|min:3',
            'description' => 'max:1000|min:3',
        ]);

        if($validator->fails()
        {
            return redirect()->back()
                             ->withErrors($validator)
                             ->withInput();
        })*/
        
       /* $request->validate([
            'title' => 'required|unique:categories|max:255|min:3',
            'description' => 'nullable|max:1000|min:3',
        ]);
*/

// dd($request->all());
        
        Category::create([
            'title' => $request->title,
            'description' => $request->description,
            'is_active'=>$request->is_active ? true : false,

        ]);
        //Session::flash('message','succesfully created');
       //dd(request()->all());
        return redirect()->route('categories.index')->withMessage('succesfully created');
        
    }

    public function show($categoryId)
    {
        //$category=Category::where('id','=',$categoryId)->first();
        $category=Category::findOrFail($categoryId);
        //return view('categories.show',['category'=>$category]);
        return view('categories.show',compact('category'));

    }
    public function edit($categoryId)
    {
        
        $category=Category::findOrFail($categoryId);
        return view('categories.edit',compact('category'));

    }

    public function update(RequestCategory $request,$categoryId)
    {
        $category=Category::findOrFail($categoryId);
        $category->update([
            'title' => $request->title,
            'description' => $request->description,
            'is_active'=>$request->is_active ? true : false,

        ]);
         return redirect()->route('categories.index')->withMessage('succesfully updated');
         //{{$category ->'is_active'? 'checked': }}
        
    }

    public function destroy($categoryId)
     {
        
      $category=Category::findOrFail($categoryId);
      $category->delete();

      return redirect()->route('categories.index')->withMessage('succesfully deleted');
     }


}
