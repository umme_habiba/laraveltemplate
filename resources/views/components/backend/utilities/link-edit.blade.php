@props(['text'=>'Edit'])
<a class="btn btn-sm btn-warning" {{ $attributes }} >
{{$text}}
</a>
