@props([
    'text'
    ])
<a {{$attributes}}>
    {{ $text }}
</a>