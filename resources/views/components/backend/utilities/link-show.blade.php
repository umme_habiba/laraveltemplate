@props(['text'=>'Show'])
<a class="btn btn-sm btn-info" {{ $attributes }}>
    {{$text}}
</a>

                                                