@props(['type'=>'submit',
 'text',
 'color'=>'default'
 ])
<button {{$type}} {{ $attributes->merge([
    'class'=>'btn btn-sm btn-'.$color]) }} >
{{ $text }}
</button>