<!DOCTYPE html>
<html lang="en">

<x-frontend.layouts.partials.head/>

<body>

    <div class="page-wrapper">
        <h1 class="d-none">Riode - Responsive eCommerce HTML Template</h1>
        <x-frontend.layouts.partials.header/>
        <!-- End Header -->
        {{$slot}}
        <!-- End Main -->
        <x-frontend.layouts.partials.footer/>
        <!-- End Footer -->
    </div>
    <!-- Sticky Footer -->
    
    <x-frontend.layouts.partials.stickyFooter/>
    <!-- Scroll Top -->
    <a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="d-icon-arrow-up"></i></a>

    <!-- MobileMenu -->
    <x-frontend.layouts.partials.mobileMenu/>

    <!-- newsletter-popup type4 -->
    <x-frontend.layouts.partials.newsletter/>


    <!-- sticky icons-->
    <x-frontend.layouts.partials.stickyIcon/>
	
	<!-- Plugins JS File -->
    <x-frontend.layouts.partials.script/>
</body>

</html>