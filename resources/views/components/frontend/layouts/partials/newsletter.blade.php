<div class="newsletter-popup newsletter-pop4 mfp-hide" id="newsletter-popup">
        <div class="newsletter-content p-relative y-50">
            <h4 class="text-uppercase text-white font-weight-bolder">Up to <span class="text-primary">20%
                    Off</span>
            </h4>
            <h2 class="font-weight-bold ls-m text-capitalize text-white mb-2">Sign up to Newsletter</h2>
            <p class="text-grey ls-s mb-6">Sing up today for free and receive updates by email..</p>
            <form action="#" method="get" class="input-wrapper input-wrapper-inline input-wrapper-round mb-5">
                <input type="email" class="form-control email" name="email" id="email2"
                    placeholder="Email address here..." required="">
                <button class="btn btn-primary text-uppercase" type="submit">Submit</button>
            </form>
            <div class="form-checkbox justify-content-center">
                <input type="checkbox" class="custom-checkbox" id="hide-newsletter-popup" name="hide-newsletter-popup"
                    required />
                <label for="hide-newsletter-popup">Don't show this popup again</label>
            </div>
        </div>
    </div>