<script src="{{asset('ui/frontend')}}/vendor/jquery/jquery.min.js"></script>
    <script src="{{asset('ui/frontend')}}/vendor/elevatezoom/jquery.elevatezoom.min.js"></script>
    <script src="{{asset('ui/frontend')}}/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <script src="{{asset('ui/frontend')}}/vendor/owl-carousel/owl.carousel.min.js"></script>
    <script src="{{asset('ui/frontend')}}/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script src="{{asset('ui/frontend')}}/vendor/isotope/isotope.pkgd.min.js"></script>
    <script src="{{asset('ui/frontend')}}/vendor/jquery.plugin/jquery.plugin.min.js"></script>
    <script src="{{asset('ui/frontend')}}/vendor/jquery.countdown/jquery.countdown.min.js"></script>
    <!-- Main JS File -->
    <script src="{{asset('ui/frontend')}}/js/main.min.js"></script>
