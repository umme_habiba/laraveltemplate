<x-backend.layouts.master>
<div class="container">
    <div class="card mb-4">
        <div class="card-header">
    <i class="fas fa-table me-1"></i>
    Category List
    <x-backend.utilities.link  text="Add New Categories" href="{{ route('categories.create')}}" />
        </div>
    </div>
    <div class="card-body">
                             

     <x-backend.alerts.message  
        type="success" 
        class="text-dark" 
        id="testId" 
        :message="session('message')"/>
        <table id="datatablesSimple">
           <thead>
                <tr>
                    <th>SL#No</th>
                    <th>Title</th>
                    <th>Is Active</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
            <tr>
               <td>{{$loop->iteration}}</td>
                <td>{{ $category->title}}</td>
                <td>{{ $category->is_active ? 'Active': 'In Active'}}</td>
                <td>{{$category->description}}</td>
                <td>
                <x-backend.utilities.link-show href="{{route('categories.show',['category'=>$category->id])}}"  />
                <x-backend.utilities.link-edit href="{{route('categories.edit',['category'=>$category->id])}}"  />
                <form method="post" action="{{route('categories.destroy',['category'=>$category->id])}}" style="display:inline">
                @csrf
                @method('delete')
                <x-backend.forms.buttons  text="Delete" color="danger" onclick="return confirm('Are you sure to delete this item')"/>
                </form>
                </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

</x-backend.layouts.master>
