<x-backend.layouts.master>
<div class="container">
<div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Category  Details
                                <x-backend.utilities.link  text="View Categories List" href="{{ route('categories.index')}}" />
                                
                            </div>
                     <div class="card-body">
                <table class="table">
                    <tr>
                        <th>Title</th>
                        <th>{{$category->title}}</th>
                    </tr>
                    <tr>
                        <th>Is Active</th>
                        <th>{{$category->is_active? 'Active' : 'In Active'}}</th>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <th>{{$category->description}}</th>
                    </tr>
                </table>
</div>
</div>
</div>                          
</x-backend.layouts.master>