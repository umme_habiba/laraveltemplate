<x-backend.layouts.master>
<div class="">
 <div class = "container">
            <form action="{{route('categories.store')}}" method="POST" enctype="multipart/form-data">

            
            @csrf
            <div class="controls">
            @if ($errors->any())
            <div class="alert alert-danger">
           <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
         </ul>
         </div>
         @endif
             
                    <div class="mb-3">
                        <div class="form-group">
                            <x-backend.forms.label text="Category Title" for="title" />
                            <x-backend.forms.input id="title" type="text" name="title" value="{{old('title')}}" />
                            @error('title')
                            <div class="form-text text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                   
                
                    <div class="form-check">
                                              
                    <input class="form-check-input" type="checkbox" name="is_active" value="1" id="is_active">
                    <label class="form-check-label" for="flexCheckChecked">
                    Is Active
                    </label>
                    </div>
               
                    <div class="mb-3">
                        <div class="form-group">
                            <x-backend.forms.label text="Category Description" for="description" />
                            <x-backend.forms.textarea id="description" name="description" :value="old('description')" />
                             @error('description')
                            <div class="form-text text-danger">{{$message}}</div>
                            @enderror
                        </div>

                    </div>
                
               
                <br>
              
                    <div class="col-md-12">
                        
                        <input type="submit" class="btn btn-primary btn-send  pt-2 mt-2 btn-block
                            " value="Save" >
                    </div>
               
          
                


        </div>
         </form>
        </div>
        </div>
</x-backend.layouts.master>
