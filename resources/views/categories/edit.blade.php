<x-backend.layouts.master>
<div class="">
 <div class = "container">
            <form action="{{route('categories.update',['category'=>$category->id])}}" method="POST" enctype="multipart/form-data">

            
            @csrf
            @method('patch')
            <div class="controls">
                
            <x-backend.alerts.errors />
             
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="CategoryTitle">Category Title</label>
                            <input id="title" type="text" name="title" value="{{old('title',$category->title)}}" class="form-control @error('title') is-invalid @enderror">
                            @error('title')
                            <div class="form-text text-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                   
                
                    <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="is_active" value="1" id="is_active" {{ $category->is_active ? 'checked' : '' }}/>
                    <label class="form-check-label" for="flexCheckChecked">
                    Is Active
                    </label>
                    </div>
               
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="description">Category Description </label>
                            <textarea id="description" name="description" class="form-control @error('title') is-invalid @enderror">{{old('description',$category->description)}}</textarea>
                            @error('description')
                            <div class="form-text text-danger">{{$message}}</div>
                            @enderror
                        </div>

                    </div>
                
               
                <br>
              
                    <div class="col-md-12">
                        
                        <input type="submit" class="btn btn-primary btn-send  pt-2 mt-2 btn-block
                            " value="Save" >
                    </div>
               
          
                


        </div>
         </form>
        </div>
        </div>
</x-backend.layouts.master>
