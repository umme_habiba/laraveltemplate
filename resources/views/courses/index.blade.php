<x-backend.layouts.master>
<div class="container">
<div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Course List
                                <a href="#">Add new course</a>
                            </div>
                        </div>
                        <div class="card-body">
                            @if (request()->session()->has('message')) 

                            <div class="alert alert-success" role="alert">
                            {{ session('message')}}
                           </div>
                             @endif

                                <table id="datatablesSimple">
                                    <thead>
                                        <tr>
                                            <th>SL#No</th>
                                            <th>Title</th>
                                            <th>Batch-No</th>
                                            <th>Class Start Date</th>
                                            <th>Class End Date</th>
                                            <th>Instructor Name</th>
                                            <th>Is Active</th>
                                            <th>Course Type</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                
                                    <tbody>
                                      @foreach($courses as $course)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{ $course->title}}</td>
                                            <td>{{ $course->batch_no}}</td>
                                            <td>{{ $course->class_start_date}}</td>
                                            <td>{{ $course->class_end_date}}</td>
                                            <td>{{ $course->instructor_name}}</td>
                                            <td>{{ $course->is_active ? 'Active': 'In Active'}}</td>
                                            <td>{{ $course->course_type}}</td>
                                            
                                           
                            
                                            <td><a>Show</a>
                                            <a>Edit</a>
                                            <a>Delete</a>
                                           </td>
                                        </tr>
                                        @endforeach
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>

</x-backend.layouts.master>
