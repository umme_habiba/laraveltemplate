<x-backend.layouts.master>
<div class="">
 <div class = "container">
        <form action="{{route('courses.store')}}" method="POST" >  
       
        @csrf
        <div class="controls">
        @if ($errors->any())
        <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
         </ul>
         </div>
         @endif
            
             
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="Title">Category Title*</label>
                            <input id="title" type="text" name="title" value="{{old('title')}}" class="form-control @error('title') is-invalid @enderror">
                            
                        </div>
                        <div class="form-group">
                        <x-backend.forms.label for="batch_no" text="Batch_no" />
                        <x-backend.forms.input id="batch_no" type="text" name="batch_no" :value="old('batch_no')" />
                        </div>

                        <div class="form-group">
                        <x-backend.forms.label for="class_start_date" text="Class_start_date" />
                        <x-backend.forms.input id="class_start_date" type="date" name="class_start_date" :value="old('class_start_date')"/>
                        </div>
                        <div class="form-group">
                        <x-backend.forms.label for="class_end_date" text="Class_End_Date" />
                        <x-backend.forms.input id="class_end_date" type="date" name="class_end_date" :value="old('class_end_date')"/>
                        </div>
                         <div class="form-group">
                        <x-backend.forms.label for="instructor_name" text="Instructor Name" />
                        <x-backend.forms.input id="instructor_name" type="text" name="instructor_name" :value="old('instructor_name')"/>
                        </div>

                    </div>
                   
                
                    <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="is_active" value="1" id="is_active">
                    <label class="form-check-label" for="flexCheckChecked">
                    Is Active
                    </label>
                    </div>
               
                    <select name="course_type" class="form-select" aria-label="Default select example">
                    <option selected>Course type</option>
                    <option value="virtual">Virtually</option>
                    <option value="physical">Phyically</option>
                    </select>

                    </div>
                
               
                <br>
              
                    <div class="col-md-12">
                        
                        <input type="submit" class="btn btn-primary btn-send  pt-2 mt-2 btn-block
                            " value="Save" >
                    </div>
               
          
                


        </div>
         </form>
        </div>
        </div>
</x-backend.layouts.master>
