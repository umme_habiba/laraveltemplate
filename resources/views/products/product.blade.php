<x-backend.layouts.master>
<div class="bg-dark">
 <div class = "container">
            <form action="dataTable.php" method="POST" enctype="multipart/form-data">

            

            <div class="controls">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ProductTitle">Product Title*</label>
                            <input id="ProductTitle" type="text" name="ProductTitle" class="form-control" placeholder="Please enter your Product Title *" required="required" data-error="Product Title is required.">
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="Price">Price *</label>
                            <input id="Price" type="text" name="Price" class="form-control" placeholder="Please enter product price *" required="required" data-error="Price is required.">
                        </div>
                    </div>
                </div>
                
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="ProductsSpecification">Products Specification *</label>
                            <textarea id="ProductsSpecification" name="ProductsSpecification" class="form-control" placeholder="Write your product specifications here" rows="4" required="required" data-error="Product Specification is important." ></textarea>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="ProductDetails">Product details </label>
                            <textarea id="ProductDetails" name="ProductDetails" class="form-control" placeholder="Write your product details here." rows="4"></textarea>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="image">Upload Product Image </label>
                            <input type="file" name ="image" class="form-control" id="image">
                        </div>  
                    </div> 

                </div>
                <br>
              
                    <div class="col-md-12">
                        
                        <input type="submit" class="btn btn-primary btn-send  pt-2 btn-block
                            " value="Save" >
                    </div>
               
          
                


        </div>
         </form>
        </div>
        </div>
</x-backend.layouts.master>
